# README #


Core407v default example for ChibiOS_2.6.8.

Connect a push button to PA0 to run test mode.

Connect an oscilloscope to PD12, PD13 to check PWM signal.

SPI2 output:

PB12 NSS

PB13 SCK

PB14 MISO

PB15 MOSI